FROM node:12.17.0-alpine

WORKDIR /usr

COPY package.json ./
COPY index.js ./index.js

RUN ls -a

RUN npm install --only=production

CMD ["npm","run","start"]